﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{

    public GameObject pausemenu;
public void ChangeScene(string scenename)
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(scenename);
    }

public void QuitGame()
    {
        Application.Quit();
    }

public void PauseGame()
    {
        pausemenu.SetActive(true);
        Time.timeScale = 0;
    }

public void Resume()
    {
        pausemenu.SetActive(false);
        Time.timeScale = 1;
    }

public void Restart(string scenename)
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(scenename);
        Time.timeScale = 1;
    }
}
