﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    //moving
    public float speed = 0.5f;
    public Rigidbody2D rigid2D;
    //jumping
    public bool grounded = true;
    public float jumpPower;
    public bool candoublejump = true;
    //hp-bar
    public Text hp_bar;
    public int hp = 5;
   

    // Start is called before the first frame update
    void Start()
    {
        hp = 5;
        hp_bar.text = "HP: " + hp.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Z) && grounded == true)
        {
            rigid2D.AddForce(new Vector2(0, jumpPower), ForceMode2D.Impulse);
            grounded = false;
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            this.gameObject.transform.position += new Vector3(speed, 0f, 0f);
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            this.gameObject.transform.position -= new Vector3(speed, 0f, 0f);
        }

        


    }
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Monster")
        {
            print("monster");
            hp -= 1;
            hp_bar.text = "HP: " + hp.ToString();
            rigid2D.AddForce(new Vector2(-2, 7), ForceMode2D.Impulse);
        }
        
        if(collision.gameObject.tag == "Trap"){
            print("trap");
            hp -= 1;
            hp_bar.text = "HP: " + hp.ToString();
            rigid2D.AddForce(new Vector2(-2, 7), ForceMode2D.Impulse);
        }

        if (collision.gameObject.tag == "Ground")
        {
            grounded = true;
        }

    }
}
